var xhr = Titanium.Network.createHTTPClient({
	onload : function() {
		// first, grab a "handle" to the file where you'll store the downloaded data
		var f = Ti.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, 'mygraphic.jpg');
		f.write(this.responseData);
		// write to the file
		Ti.App.fireEvent('graphic_downloaded', {
			filepath : f.nativePath
		});
	},
	timeout : 10000
});
xhr.open('GET', $.image.image);
xhr.send();
Ti.App.addEventListener('graphic_downloaded', function(e) {
	// you don't have to fire an event like this, but perhaps multiple components will
	// want to know when the image has been downloaded and saved
	Ti.API.info(e.filepath);
	$.image.image = e.filepath;
});

function shareImage(e) {
	var fileToShare = null;

	// generate image from screen
	if (OS_ANDROID) {
		var img = $.image.toImage().media;
		fileToShare = Titanium.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, 'mygraphic.jpg');
	} else {
		var img = $.newimage.toImage();
		fileToShare = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'tempimage.jpg');
	}
	//fileToShare.write(img);
	
	Ti.API.info("test : " + $.image.image);
	
	//

	// share image
	require('com.alcoapps.socialshare').share({
		status : 'http://www.google.com',
		image : fileToShare.nativePath,
		androidDialogTitle : 'Sharing is caring!!!',
		view : $.label2
	});
}

function shareText(e) {
	// share text status
	require('com.alcoapps.socialshare').share({
		status : 'This is the status to sahre',
		androidDialogTitle : 'Caption!!!',
		view : $.label1
	});
}

function shareImageWidget(e) {
	var fileToShare = null;

	// generate image from screen
	if (OS_ANDROID) {
		var img = $.newimage.toImage().media;
		fileToShare = Titanium.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, 'tempimage.jpg');
	} else {
		var img = $.newimage.toImage();
		fileToShare = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'tempimage.jpg');
	}
	fileToShare.write(img);
	//

	// share image
	var socialWidget = Alloy.createWidget('com.alcoapps.socialshare');
	socialWidget.share({
		status : 'This is the status to share',
		image : fileToShare.nativePath,
		androidDialogTitle : 'Sharing is caring!!!',
		view : $.label4
	});
}

function shareTextWidget(e) {
	// share text status
	var socialWidget = Alloy.createWidget('com.alcoapps.socialshare');
	socialWidget.share({
		status : 'This is the status to share',
		androidDialogTitle : 'Caption!!!',
		view : $.label3
	});
}

$.index.open();
